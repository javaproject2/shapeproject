/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.shapeproject;

/**
 *
 * @author tud08
 */
public class Square {
    private double s;
    public Square(double s){
        this.s = s;
    }
    public double calArea(){
        return s*s;
    }
    public void setS(double s){
        if (s<=0){
            System.out.println("Error size must more than zero!!");
            return;
        }
        this.s = s;
    }
    public double getS(){
        return this.s;
    }
    public String toString(){
        return "Area of Square1 = "+this.getS()+" is "+this.calArea();
    }
}
