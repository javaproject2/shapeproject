/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.shapeproject;

/**
 *
 * @author tud08
 */
public class Triangle {
    private double base;
    private double high;
    
    public Triangle(double b,double h){
        base = b;
        high = h;
    }
    public double calArea(){
        return base*high/2;
    }
    public void setBH(double b,double h){
        if (b==0||h==0){
            System.out.println("Error size must more than zero!!");
            return;
        }
        base = b;
        high = h;
    }
    public double getBase(){
        return base;
    }
    public double getHigh(){
        return high;
    }
    public String toString(){
        return "Area of Triangle1 = "+"Base "+this.getBase()+" High "+this.getHigh()+" is "+this.calArea();
    }
}
