/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.shapeproject;

/**
 *
 * @author tud08
 */
public class Rectangle {
    private double w;
    private double l;
    
    public Rectangle(double w,double l){
        this.w = w;
        this.l = l;
    }
    public double calArea(){
        return this.w*this.l;
    }
    public void setWL(double w,double l){
        if (w>=l||w==0||l==0){
            System.out.println("Error size must more than zero!!");
            return;
        }
        this.w = w;
        this.l = l;
    }
    public double getL(){
        return this.l;
    }
    public double getW(){
        return this.w;
    }
    public String toString(){
        return "Area of Rectangle1 = "+"Wide "+getW()+" Long "+getL()+" is "+calArea();
    }
}
